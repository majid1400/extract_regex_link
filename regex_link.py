import re
from multiprocessing import Process
import pandas as pd
import time

"""
For more information multiprocessing: https://goo.gl/bqzRCz
"""


def regex_link():
    x = []
    x.append(("link"))
    with open('ch.csv', encoding="utf-8") as texts:
        for text in texts:
            matchText = re.search(r'((https?):((//)|(\\\\))+([\w\d:#@%/;$()~_?\+-=\\\.&](#!)?)*)', text)
            if matchText:
                print(matchText[0])
                x.append((matchText[0]))
                a = pd.DataFrame(x)
    a.to_csv('tw' + str(time.time()) + '.csv', encoding='utf-8', sep=',')


# Create multiprocessing
if __name__ == '__main__':
    p = Process(target=regex_link)
    p.start()
    p.join()
    print('Done')
