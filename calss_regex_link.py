import re
from multiprocessing import Process
import time
import pandas as pd


class Regex:
    def run_regex(self, address, pattern, topCsv):
        x = self.one_append(topCsv)
        with open(address, encoding="utf-8") as texts:
            self.iterator_text(texts, x)
            self.save_to_csv(x)

    def iterator_text(self, texts, x):
        for text in texts:
            if self.reg(pattern, text):
                print(self.reg(pattern, text)[0])
                self.file_append(self.reg(pattern, text)[0], x)

    def reg(self, pattern, text):
        matchText = re.search(pattern, text)
        return matchText

    def one_append(self, topCsv):
        x = []
        x.append(topCsv)
        return x

    def file_append(self, file, x):
        x.append(file)

    def save_to_csv(self, x):
        a = pd.DataFrame(x)
        a.to_csv('tw' + str(time.time()) + '.csv', encoding='utf-8', sep=',')


if __name__ == '__main__':
    regex = Regex()
    pattern = r'((https?):((//)|(\\\\))+([\w\d:#@%/;$()~_?\+-=\\\.&](#!)?)*)'
    regex_link = regex.run_regex('1.txt', pattern, 'link')
    p = Process(target=regex_link)
    p.start()
    p.join()
    print('Done')
